<?php
// $Id$

/**
 * @file
 * Admin page callback file for the openid_restrict module.
 */

/**
 * Form Builder: Openid Admin settings
 */
function openid_restrict_settings() {

  // build form
  $form = array();
  
  // Title
  $form['authorized'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authorized Domains'),
    '#description' => t('Uncheck domain(s) to deauthorized.'),
  );
  
  // get domains
  $empty = TRUE;
  $result = db_query('SELECT * FROM {openid_restrict}');
    
  // Iterate domains
  while ($item = db_fetch_object($result)) {
    $empty = FALSE;
    $form['authorized']['checkbox_' . $item->did] = array(
      '#title' => $item->domain,
      '#type' => 'checkbox',
      '#default_value' => 1,
    );
  }
  
  // Print message if there are no authorized domains
  if ($empty) {
    $form['message'] = array(
      '#type' => 'markup',
      '#value' => t('You specified no authorized domains.  All domains are permitted.'),
    );
  }  
  
  // add domain field
  $form['domain'] = array(
    '#title' => t('Authorize a Domain'),
    '#type' => 'textfield',
    '#description' => t('Enter a valid url'),
  );
  
  // Submit button
  $form['submit'] = array(
    '#value' => t('Submit'),
    '#type' => 'submit',
  );
  
  return $form;
}


/**
 * Validate Domain
 */
function openid_restrict_settings_validate($form, &$form_state) {
  
  $domain = $form_state['values']['domain'];
  if (!empty($domain) && !valid_url($domain, TRUE)) {
    form_set_error('domain', t('This is not a valid domain.'));
  }
  
  // get domains
  $result = db_query('SELECT * FROM {openid_restrict}');
  
  // Make sure this domains doesn't already exist in the database
  while ($item = db_fetch_object($result)) {
    if (strpos($domain, $item->domain) !== FALSE) {
      // This domain is authorized, no need to look further
      form_set_error('domain', 'This domain is already authorized.');
    }
  }

}

/**
 * Submit form handler
 */
function openid_restrict_settings_submit($form, &$form_state) {
  
  // Iterate checkboxes and remove unchecked values
  $result = db_query('SELECT * FROM {openid_restrict}');
  
  // Make sure this domains doesn't already exist in the database
  while ($item = db_fetch_object($result)) {
    if ($form_state['values']['checkbox_' . $item->did] == 0) {
      // If the box is unchecked, then remove this domain from the database
      db_query('DELETE FROM {openid_restrict} WHERE did="%s"', $item->did);
      drupal_set_message($item->domain . ' has been deauthorized.');
    }
  }  
  
  // Insert new authorized domain if the textfield is populated
  $record = new stdClass();
  $record->domain = $form_state['values']['domain'];
  if (!empty($record->domain)) {
    drupal_write_record('openid_restrict', $record);
    drupal_set_message($record->domain . t(' has been authorized.  Any domain not listed will be blocked.'));
  }  
}